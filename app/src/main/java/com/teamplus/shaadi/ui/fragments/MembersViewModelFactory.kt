package com.teamplus.shaadi.ui.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.teamplus.shaadi.data.repositories.MembersRepository

@Suppress("UNCHECKED_CAST")
class MembersViewModelFactory(
    private val repository: MembersRepository
) : ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MembersViewModel(repository) as T
    }

}