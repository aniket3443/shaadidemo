package com.teamplus.shaadi.ui.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.teamplus.shaadi.data.models.Result
import com.teamplus.shaadi.data.repositories.MembersRepository
import com.teamplus.shaadi.util.Coroutines
import kotlinx.coroutines.Job

class MembersViewModel(
    private val repository: MembersRepository
) : ViewModel() {
    private lateinit var job: Job
    private val membersMutableLiveData = MutableLiveData<List<Result>>()
    private val appsListMutableLiveData = mutableListOf<Result>()

    val membersLiveData: LiveData<List<Result>>
        get() = membersMutableLiveData

    fun getMembers() {
        job = Coroutines.ioThenMain(
            { repository.getMembers() },
            {
                membersMutableLiveData.value = it?.results
                appsListMutableLiveData.clear()
                it?.results?.forEach { result ->
                    appsListMutableLiveData.add(result)
                }
            }
        )
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}
