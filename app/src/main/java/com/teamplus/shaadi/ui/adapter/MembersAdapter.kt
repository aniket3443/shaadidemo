package com.teamplus.shaadi.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.teamplus.shaadi.R
import com.teamplus.shaadi.data.models.Result
import com.teamplus.shaadi.databinding.ItemRowMemberBinding
import com.teamplus.shaadi.ui.listener.RecyclerViewClickListener

class MembersAdapter(
    private val membersList: List<Result>,
    private val listener: RecyclerViewClickListener,
    private val context: Context
) : RecyclerView.Adapter<MembersAdapter.MembersViewHolder>() {

    override fun getItemCount() = membersList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MembersViewHolder(
            DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_row_member,
            parent,
            false
            )
        )

    override fun onBindViewHolder(holder: MembersViewHolder, position: Int) {
        holder.itemRowMemberBinding.result = membersList[position]
        val url = membersList[position].picture.medium
        Glide.with(context)
            .load(url)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.ic_account_circle_24px)
                    .centerCrop()
                    .dontAnimate()
                    .dontTransform()
            )
            .into(holder.itemRowMemberBinding.memberImageView)

        holder.itemRowMemberBinding.declineButton.setOnClickListener {
            listener.onRecyclerViewItemClick(
                holder.itemRowMemberBinding.declineButton,
                membersList[position],
                holder.itemRowMemberBinding.userSelectionTextView
            )
            userPerformActionOnCard(holder.itemRowMemberBinding, context.getString(R.string.declined))
        }
        holder.itemRowMemberBinding.acceptButton.setOnClickListener {
            listener.onRecyclerViewItemClick(
                holder.itemRowMemberBinding.acceptButton,
                membersList[position],
                holder.itemRowMemberBinding.userSelectionTextView
            )
            userPerformActionOnCard(holder.itemRowMemberBinding, context.getString(R.string.accepted))
        }

    }
    private fun userPerformActionOnCard(
        itemRowMemberBinding: ItemRowMemberBinding,
        operationString: String
    ) {
        itemRowMemberBinding.userSelectionTextView.visibility = View.VISIBLE
        itemRowMemberBinding.userSelectionTextView.text = operationString
        itemRowMemberBinding.acceptButton.visibility = View.GONE
        itemRowMemberBinding.declineButton.visibility = View.GONE
    }

    inner class MembersViewHolder(
        val itemRowMemberBinding : ItemRowMemberBinding
    ) : RecyclerView.ViewHolder(itemRowMemberBinding.root)

}