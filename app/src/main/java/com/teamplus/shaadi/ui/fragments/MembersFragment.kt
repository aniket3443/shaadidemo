package com.teamplus.shaadi.ui.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textview.MaterialTextView
import com.teamplus.shaadi.R
import com.teamplus.shaadi.data.models.Result
import com.teamplus.shaadi.data.network.Api
import com.teamplus.shaadi.data.repositories.MembersRepository
import com.teamplus.shaadi.databinding.MembersFragmentBinding
import com.teamplus.shaadi.ui.adapter.MembersAdapter
import com.teamplus.shaadi.ui.listener.RecyclerViewClickListener
import kotlinx.android.synthetic.main.members_fragment.*

class MembersFragment : Fragment(), RecyclerViewClickListener {

    private lateinit var membersViewModelFactory: MembersViewModelFactory
    private lateinit var membersViewModel: MembersViewModel
    private lateinit var membersFragmentBinding: MembersFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        membersFragmentBinding = MembersFragmentBinding.inflate(inflater, container, false)
        return membersFragmentBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        membersFragmentBinding.progressBar.visibility = View.VISIBLE

        val api = Api()
        val repository = MembersRepository(api)

        membersViewModelFactory = MembersViewModelFactory(repository)
        membersViewModel =
            ViewModelProvider(this, membersViewModelFactory).get(MembersViewModel::class.java)

        if (isNetworkAvailable(requireContext())) {
            membersViewModel.getMembers()
        } else {
            //TODO: pull down to refresh list
            membersFragmentBinding.errorTextView.visibility = View.VISIBLE
            membersFragmentBinding.progressBar.visibility = View.GONE
        }

        membersViewModel.membersLiveData.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { members ->
                recycler_view_members.also {
                    it.layoutManager = LinearLayoutManager(requireContext())
                    it.setHasFixedSize(true)
                    it.adapter = MembersAdapter(members, this, requireContext())
                    membersFragmentBinding.progressBar.visibility = View.GONE
                }
            })

    }

    override fun onRecyclerViewItemClick(view: View, result: Result, userSelectionTextView: MaterialTextView) {
        when (view.id) {
            R.id.acceptButton -> {
                Toast.makeText(context,"Accepted",Toast.LENGTH_SHORT).show()
                //TODO: handle database entry
            }
            R.id.declineButton -> {
                Toast.makeText(context,"Declined",Toast.LENGTH_SHORT).show()
                //TODO: handle database entry
            }
        }
    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork ?: return false
        val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
        return when {
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }


}