package com.teamplus.shaadi.ui.listener

import android.view.View
import com.google.android.material.textview.MaterialTextView
import com.teamplus.shaadi.data.models.Result

interface RecyclerViewClickListener {
    fun onRecyclerViewItemClick(view: View, result: Result, declineButton: MaterialTextView)
}