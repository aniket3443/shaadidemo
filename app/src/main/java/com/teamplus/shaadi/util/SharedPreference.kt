package com.teamplus.shaadi.util

import android.content.Context
import androidx.annotation.StringRes
import androidx.preference.PreferenceManager

object SharedPreferenceManager {

    fun saveStringPreference(context: Context, @StringRes prefKeyId: Int, value: String) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putString(context.getString(prefKeyId), value)
            .apply()
    }

    fun getStringPreference(context: Context, @StringRes prefKeyId: Int): String? {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getString(context.getString(prefKeyId), null)
    }

}