package com.teamplus.shaadi.data.models
import com.google.gson.annotations.SerializedName

data class ResultsData (

	@SerializedName("results") val results : List<Result>,
	@SerializedName("info") val info : Info
)