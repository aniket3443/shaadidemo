package com.teamplus.shaadi.data.repositories

import com.teamplus.shaadi.data.network.Api

class MembersRepository(
    private val api: Api
) : SafeApiRequest() {
    
    suspend fun getMembers() = apiRequest { api.getMembers() }

}