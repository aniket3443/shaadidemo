package com.teamplus.shaadi.data.network

import com.teamplus.shaadi.data.models.ResultsData
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit


interface Api {

    @GET("api/?results=10")
    suspend fun getMembers() : Response<ResultsData>

    companion object{
        operator fun invoke() : Api {
            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://randomuser.me/")
                .callFactory(okHttpClient)
                .build()
                .create(Api::class.java)
        }
    }
}
